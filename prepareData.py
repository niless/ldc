#Author: Efsun Kayi
#python prepareData.py path_to_data_folder
#Prepares gold data for targeted and untargeted tasks
import pickle, os, csv, sys, pandas
from collections import Counter
#from sets import Set
import xml.etree.ElementTree as ET

def getLabels(sentiment):
  sec=[]
  for f in os.listdir(sentiment):
      if f.endswith(".tab"):
        with open(sentiment+f, 'r') as csvfile:
            sentimentreader = csv.DictReader(csvfile, delimiter='\t')
            for row in sentimentreader:
                row.pop('user_id')
                row.pop('source')
                if row['target']=='frame':
                  row['target']='none'
                sec.append(row)        
  df = pandas.DataFrame.from_records(sec)
  df.drop_duplicates(inplace=True)
  return df

def getSegments(monolingual):
  segments = []
  for fPath in os.listdir(monolingual):
      tree = ET.parse(monolingual+fPath)
      root = tree.getroot()
      for doc in root:
         for seg in root.iter('SEG'):
            docSegment = {'doc_id': doc.attrib.get('id'), 'emotion_value':'na','frame_id':'none','segment': seg.attrib.get('id'),'sentiment_value':'na', 'target':'none'}
            segments.append(docSegment)   
  df = pandas.DataFrame.from_records(segments)
  return df


def main():
  dataPath = sys.argv[1]
  sentimentDF = getLabels(dataPath+'/sentiment/')
  segmentsDF = getSegments(dataPath+'/monolingual_text/ltf/')

  #New version
  dfUntargetedAll = pandas.merge(sentimentDF,segmentsDF,on=['doc_id','segment'],how='right',indicator=True)
  dfUntargetedAll.drop(dfUntargetedAll.columns[[6,7,8,9,10]],axis=1,inplace=True) #emotion_value_y, sentiment_value_y
  dfUntargetedAll.rename(columns={'doc_id': 'DocumentID','segment':'SegmentID','sentiment_value_x':'Predicted SentimentClass','emotion_value_x':'Predicted EmotionClass','frame_id_x':'Predicted FrameID','target_x':'Predicted EntityID'}, inplace=True)
  dfUntargetedAll.fillna('none',inplace=True)
  dfUntargetedAll.to_csv('untargeted-test-all.tsv',sep='\t',index=False) 

  sentimentDF.rename(columns={'doc_id': 'DocumentID','segment':'SegmentID','sentiment_value':'Predicted SentimentClass','emotion_value':'Predicted EmotionClass','frame_id':'Predicted FrameID','target':'Predicted EntityID'}, inplace=True)
  sentimentDF.fillna('none',inplace=True)
  sentimentDF.to_csv('targeted-test-all.tsv',sep='\t',index=False)

if __name__ == "__main__":
    main()
##Description: Scoring script for LORELEI SEC Pilot Evaluation

The SEC pilot evaluation will measure how well systems perform in identifying and classifying sentiment polarity (positive/negative) and/or emotion (anger and/or fear) towards a situation frame (SF) and/or towards entities that are part of the situation frame (SFE). 

The input to the system will be:

1. Input documents
2. Situation Frame annotations over the input documents (including SF type and SF entities)

The pilot is configured modularly such that participants may contribute to any of the various aspects of the evaluation

## List of Evaluation tasks:

1. Targeted sentiment towards frame
2. Targeted emotion towards frame
3. Targeted sentiment towards entity
4. Targeted emotion towards entity
5. Untargeted sentiment
6. Untargeted emotion

Identification and classification scores are reported for each task. Identification refers to identifying the existence of {Sentiment|Emotion} without referring to its class whereas classification refers to identification of the class of {Sentiment|Emotion}. 

* Possible Sentiment class values are {positive, negative, both, none}
* Possible Emotion class values are {anger, fear, both, none}. 
* Segment IDs not associated with any sentiment or emotion will have assumed none values for all fields.

For more details on the evaluation tasks and scoring, please refer to the SEC pilot evaluation document and [scoring document](./sentiment-eval-scoring.pdf).

##Run like this:

python prepareData.py <path to data folder> 

(e.g. /LDC2017E21_LORELEI_SEC_Pilot_Evaluation_Data_Annotation/data/)

python seceval.py <gold file> <prediction file> <task number>

Use targeted-test.tsv for Tasks 1-4 and untargeted-test.tsv for Task 5-6 for gold file.

## Formats:

Gold and output files should be in the following tab-separated format:

DocumentID      SegmentID       Predicted SentimentClass        Predicted EmotionClass	Predicted FrameID       Predicted EntityID


##Software Requirements
This code is *only* tested with the software versions mentioned below.

* Python 3.6
* Pandas 0.19 *(minimum 0.17 is required)*

##Contact

For questions or info, please contact Efsun Kayi (efsun@gwu.edu)
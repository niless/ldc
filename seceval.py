# Author: Efsun Kayi (efsun@gwu.edu)
from __future__ import division
import pandas as pd
import numpy as np
import sys, csv
#from sets import Set
import pickle

def computePRF(tp,fp,fn):
    #print tp, fp, fn
    div_error=False
    if tp==0:
        precision = 0
        recall = 0
        F1='na'
        div_error=True
    else:    
        precision = tp/(tp+fp) 
        recall = tp/(tp+fn)
        F1 = (2*precision*recall)/(precision+recall)   
    return  {'Precision':precision,'Recall':recall,'F-Score' : F1}

def assignScores(tp,fp,fn,scores):
    scores['TP'] = scores['TP']+tp
    scores['FP'] = scores['FP']+fp
    scores['FN'] = scores['FN']+fn

def alternativeScore(both,goldOnly,testOnly,cols,labels,altscores,identify,nopenalty):
    dfg = both.groupby(cols) 
    for name, group in dfg:
        gold = set(group[labels[0]])
        test = set(group[labels[1]])
        #print gold, test
        if identify:
            if len(gold)>1: #both pos and neg
                gold=set(['pos'])
            if len(test)>1: #both pos and neg
                test=set(['pos'])    
        tp = gold.intersection(test)
        fp = test.difference(gold)
        fn = gold.difference(test)
        #print tp, fp, fn
        for c in tp:
            if len([s for s in altscores if s['Class'] == c])>0:
                [s for s in altscores if s['Class'] == c][0]['tp']+=1
        for c in fp:
            if len([s for s in altscores if s['Class'] == c])>0:
                if nopenalty:
                    if gold == set(['none']) and c!='none':
                        pass
                    else:    
                        [s for s in altscores if s['Class'] == c][0]['fp']+=1
                else:
                    [s for s in altscores if s['Class'] == c][0]['fp']+=1    
        for c in fn:
            if len([s for s in altscores if s['Class'] == c])>0:
                [s for s in altscores if s['Class'] == c][0]['fn']+=1        

    micro = {'TP':0,'FP':0,'FN':0}
    for s in altscores:
        #print s['Class']
        s['fp'] += len(testOnly[testOnly[labels[1]]==s['Class']])
        s['fn'] += len(goldOnly[goldOnly[labels[0]]==s['Class']])
        #if s['Class']!='none':
        if not identify:
            assignScores(s['tp'],s['fp'],s['fn'],micro)   
        prf = computePRF(s['tp'],s['fp'],s['fn']) 
        s['Precision'] = prf['Precision']
        s['Recall'] = prf['Recall']
        s['F-Score'] = prf['F-Score']
    return micro          
            
def filterNull(df,field):
    df = df[~df[field].isin(['none','na'])]
    df = df[df[field].notnull()]
    return df

def f_lower(x):
    if isinstance(x,str): return str.lower(x).strip()

def macroAvg(scores, none):
    if none:
        avgPrecision = np.mean([s['Precision'] for s in scores if s['Class']!='none'])
        avgRecall = np.mean([s['Recall'] for s in scores if s['Class']!='none']) 
    else:
        avgPrecision = np.mean([s['Precision'] for s in scores])
        avgRecall = np.mean([s['Recall'] for s in scores])    
    if (avgPrecision+avgRecall)!=0:
        avgFscore = (2*avgPrecision*avgRecall)/(avgPrecision+avgRecall)
    else:
        avgFscore = 'na'
    return avgPrecision, avgRecall, avgFscore

def averageScores(scores,allscores):
    #print scores
    avgPrecision, avgRecall, avgFscore = macroAvg(scores,True)
    avgPrecisionNone, avgRecallNone, avgFscoreNone = macroAvg(scores,False)
    prfNone = computePRF(allscores['TP'],allscores['FP'],allscores['FN'])
    allscores['TP'] = allscores['TP']- [s for s in scores if s['Class'] == 'none'][0]['tp']
    allscores['FP'] = allscores['FP']- [s for s in scores if s['Class'] == 'none'][0]['fp']
    allscores['FN'] = allscores['FN']- [s for s in scores if s['Class'] == 'none'][0]['fn']
    prf = computePRF(allscores['TP'],allscores['FP'],allscores['FN'])
    macro = {'Precision':avgPrecision,'Recall':avgRecall,'F-Score' : avgFscore, 'Class':'Macro Average'} 
    micro = {'Precision':prf['Precision'],'Recall':prf['Recall'],'F-Score' : prf['F-Score'], 'Class':'Micro Average'}
    macro_none = {'Precision':avgPrecisionNone,'Recall':avgRecallNone,'F-Score' : avgFscoreNone, 'Class':'Macro Average with none'} 
    micro_none = {'Precision':prfNone['Precision'],'Recall':prfNone['Recall'],'F-Score' : prfNone['F-Score'], 'Class':'Micro Average with none'}
    return [macro,micro,macro_none, micro_none]  

def checkLabels(df,labels,values):
    l = labels[0]
    l = l[0:l.index('_')]
    setLabels = set(df[l])
    setValues = set(values)
    if not setLabels.issubset(setValues):
        return False
    return True    

def prepDF(gold,test,label,values):
    cl = label.split('_')[0]
    v  = [item for item in values if item  != 'none']
    dfGold = gold.copy()
    dfTest = test.copy()
    dfGold.loc[dfGold[cl].isin(v),cl]='pos'
    dfTest.loc[dfTest[cl].isin(v),cl]='pos'
    #dfGold.loc[dfGold[cl]=='none',cl]='neg'
    #dfTest.loc[dfTest[cl]=='none',cl]='neg'
    return dfGold, dfTest

def mergeDF(dfGold,dfTest,keys,label,values):
    dfGoldTestLeft = pd.merge(dfGold,dfTest,on=keys,how='left',indicator=True)
    dfGoldTestRight = pd.merge(dfGold,dfTest,on=keys,how='right',indicator=True)   
    both = dfGoldTestLeft[dfGoldTestLeft['_merge']=='both']
    goldOnly = dfGoldTestLeft[dfGoldTestLeft['_merge']=='left_only']
    testOnly = dfGoldTestRight[dfGoldTestRight['_merge']=='right_only'] 
    return both, goldOnly, testOnly    

def expandBoth(df,label,values):
    l = label.split('_')[0]
    cp = df[df[l]=='both'].copy()
    df.loc[df[l]=='both',l]=values[0]
    cp[l]=values[1]
    df = df.append(cp)
    return df

def prepCols(score,cols):
    #[score.remove(s) for s in score if s['Class']=='none'] 
    for k in cols:
        [i.pop(k, None) for i in score]
        
def main():
    #Print usage info
    if len(sys.argv)!=4:
        print("Program expects three command line arguments as below:")
        print("python seceval.py gold_file.txt prediction_file.txt taskno")
        sys.exit(1)
    dfGold = pd.read_csv(sys.argv[1],delimiter="\t")
    dfTest = pd.read_csv(sys.argv[2],delimiter="\t")

    dfGold =  dfGold.applymap(f_lower)
    dfTest =  dfTest.applymap(f_lower) 
    dfTest.drop_duplicates(inplace=True)
    
    task = int(sys.argv[3])
    noGold = len(dfGold)
    noTest = len(dfTest)
    noSegments = len(dfGold.groupby(['DocumentID', 'SegmentID']))
    columnNames = set(['DocumentID','SegmentID','Predicted SentimentClass','Predicted EmotionClass','Predicted FrameID','Predicted EntityID'])
    goldColumns = set(dfGold.columns)
    testColumns = set(dfTest.columns)
    if goldColumns!=columnNames or testColumns!=columnNames:
        print('Both gold and test should consist of the following columns: DocumentID, SegmentID, Predicted SentimentClass, Predicted EmotionClass, Predicted FrameID, Predicted EntityID')
        sys.exit(1)
    if task<1 or task>6:
        print("Task number should be in between 1 and 6.")
        sys.exit(1)   

    nopenalty=False
    values=['positive','negative','none','both']
    labels = ['Predicted SentimentClass_x','Predicted SentimentClass_y']
    keys=['DocumentID','SegmentID','Predicted FrameID']
    keysAlt = ['DocumentID','Predicted FrameID']

    if task==2: #targeted emotion toward frameid
        labels = ['Predicted EmotionClass_x','Predicted EmotionClass_y']
        values=['anger','fear','none','both']
    elif task==3: #targeted sentiment toward entityid 
        keys = ['DocumentID','SegmentID','Predicted EntityID']
        keysAlt = ['DocumentID','Predicted EntityID']
    elif task==4: #targeted emotion toward entityid
        keys = ['DocumentID','SegmentID','Predicted EntityID']
        keysAlt = ['DocumentID','Predicted EntityID']
        labels = ['Predicted EmotionClass_x','Predicted EmotionClass_y']
        values=['anger','fear','none','both']
    elif task==5: #untargeted sentiment
        keys = ['DocumentID','SegmentID']
        nopenalty=True
    elif task==6: #untargeted emotion
        keys = ['DocumentID','SegmentID']
        labels = ['Predicted EmotionClass_x','Predicted EmotionClass_y']
        values=['anger','fear','none','both']
        nopenalty = True   
    
    
    if task==1 or task==2:# frame target
        dfGold = filterNull(dfGold,'Predicted FrameID') 
        dfTest = filterNull(dfTest,'Predicted FrameID') 
    elif task==3 or task==4: # entity target
        dfGold = filterNull(dfGold,'Predicted EntityID') 
        dfTest = filterNull(dfTest,'Predicted EntityID')

    if not checkLabels(dfTest,labels,values):
        print('Error in class labels. Following values are valid')
        print(values)
        sys.exit(1)         
   
    dfTest = expandBoth(dfTest,labels[0],values[:2])      
    both, goldOnly, testOnly = mergeDF(dfGold,dfTest,keys,labels,values)
    goldIdentify, testIdentify = prepDF(dfGold,dfTest,labels[0],values)

    # Multi-label  
    identifyScore = [{'Class':'pos','tp':0,'fp':0,'fn':0}] 
    bothIdentify, goldOnlyIdentify, testOnlyIdentify = mergeDF(goldIdentify,testIdentify,keys,labels,values)
    alternativeScore(bothIdentify,goldOnlyIdentify,testOnlyIdentify,keys,labels,identifyScore,True,nopenalty)
    print("Identification score: "+str(identifyScore))
    classifyMulti = []
    for c in values:
        if c!='both':
            classifyMulti.append({'Class':c,'tp':0,'fp':0,'fn':0})
    classifyMultiMicro = alternativeScore(both,goldOnly,testOnly,keys,labels,classifyMulti,False,nopenalty)   
    macroClassMulti, microClassMulti, macroClassMultiNone, microClassMultiNone = averageScores(classifyMulti,classifyMultiMicro)
    print("Multi-label classiciation scores: " + str(classifyMulti))
    print("Multi-label classification average scores: " + str(macroClassMulti))
    print(microClassMulti)
    print("Multi-label classification average scores with none included: " + str(macroClassMultiNone))
    print(microClassMultiNone)
   
    if task<5: #targeted tasks calculate alternative scoring   
        bothAlt, goldOnlyAlt, testOnlyAlt = mergeDF(dfGold,dfTest,keysAlt,labels,values)
        altIdentification = [{'Class':'pos','tp':0,'fp':0,'fn':0}] 
        bothAltIdentify, goldOnlyAltIdentify, testOnlyAltIdentify = mergeDF(goldIdentify,testIdentify,keysAlt,labels,values)
        alternativeScore(bothAltIdentify,goldOnlyAltIdentify,testOnlyAltIdentify,keysAlt,labels,altIdentification,True,nopenalty)   
        altClassification = []
        for c in values:
            if c!='both':
                altClassification.append({'Class':c,'tp':0,'fp':0,'fn':0})
        altMicro = alternativeScore(bothAlt,goldOnlyAlt,testOnlyAlt,keysAlt,labels,altClassification,False,nopenalty)
        macroClassAlt, microClassAlt, macroClassAltNone, microClassAltNone = averageScores(altClassification,altMicro)
        print("Alternative Identification results for task "+str(task)+":")
        print(altIdentification)
        print("Alternative classification results for task "+str(task)+":")
        print(altClassification)
        print('Alternative average classification scores: ' +str(macroClassAlt))
        print(microClassAlt)
 
    outfname = sys.argv[2]
    outfname = outfname[0:outfname.index('.')] + '-output.tsv'
    prepCols(identifyScore,['tp','fp','fn','Class']) 
    prepCols(classifyMulti,['tp','fp','fn']) 
    with open(outfname,'w') as fout:
        dw = csv.DictWriter(fout, fieldnames=classifyMulti[0].keys() ,delimiter='\t')
        dw.writeheader()
        dw.writerow({'Precision':'','Recall':'','F-Score':'Identification Score','Class':''})
        dw.writerows(identifyScore)
        dw.writerow({'Precision':'','Recall':'','F-Score':'Classification Score','Class':''})
        dw.writerows(classifyMulti)
        dw.writerow(macroClassMulti)
        dw.writerow(macroClassMultiNone)
        dw.writerow(microClassMulti)
        dw.writerow(microClassMultiNone)
        if task<5:
            prepCols(altIdentification,['tp','fp','fn','Class']) 
            prepCols(altClassification,['tp','fp','fn']) 
            dw.writerow({'Precision':'','Recall':'','F-Score':'Alternative Identification Score','Class':''})
            dw.writerows(altIdentification)
            dw.writerow({'Precision':'','Recall':'','F-Score':'Alternative Classification Score','Class':''})
            dw.writerows(altClassification)
            dw.writerow(macroClassAlt)
            dw.writerow(macroClassAltNone)
            dw.writerow(microClassAlt)
            dw.writerow(microClassAltNone)

if __name__ == '__main__':
    main()
